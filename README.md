Repo para la actualización de archivos de configuración de usuario en notebook Ubuntu

Autor: SDR
Versión: 0.2
Fecha: 06 Abril 2024

- LVM template bash script
- apt installs de libs y apps en general
- apt installs para CUDA
- apt installs de libs y apps R y RStudio
- rsyncs aliases para file backups
